(**************************************************************************)
(*                                                                        *)
(*  This file is part of Frama-C.                                         *)
(*                                                                        *)
(*  Copyright (C) 2007-2022                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

let should_memorize_function f =
  Parameters.ResultsAll.get () &&
  not (Cil_datatype.Fundec.Set.mem
         f (Parameters.NoResultsFunctions.get ()))

let () = Db.Value.no_results :=
    (fun fd -> not (should_memorize_function fd)
               || not (Parameters.Domains.mem "cvalue"))

(* Signal that some results are not stored. The gui, or some calls to
   Db.Value, may fail ungracefully *)
let no_memoization_enabled () =
  not (Parameters.ResultsAll.get ()) ||
  not (Parameters.NoResultsFunctions.is_empty ())



(*
Local Variables:
compile-command: "make -C ../../../.."
End:
*)
